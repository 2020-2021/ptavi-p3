#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax.handler import ContentHandler
from xml.sax import make_parser

class SmallSMILHandler(ContentHandler):

    def __init__(self):

        self.dic_raiz = {}
        self.dic_zona = {}
        self.dic_imag = {}
        self.dic_sonido = {}
        self.dic_texts = {}

        self.listaa = []

    def startElement(self, name, atrib):

        if name == 'raiz-layout':
            
            self.dic_raiz['eti'] = name
            self.dic_raiz['ancho'] = atrib.get('ancho', "")
            self.dic_raiz['alto'] = atrib.get('alto', "")
            self.dic_raiz['bg-color'] = atrib.get('background-color', "")
            self.listaa.append(self.dic_raiz.copy())

        elif name == 'zona':

            self.dic_zona['eti'] = name
            self.dic_zona['id'] = atrib.get('id', "")
            self.dic_zona['arriba'] = atrib.get('arriba', "")
            self.dic_zona['abajo'] = atrib.get('abajo', "")
            self.dic_zona['dcha'] = atrib.get('dcha', "")
            self.dic_zona['izq'] = atrib.get('izq', "")
            self.listaa.append(self.dic_zona.copy())

        elif name == 'imag':

            self.dic_imag['eti'] = name
            self.dic_imag['source'] = atrib.get('source', "")
            self.dic_imag['zona'] = atrib.get('zona', "")
            self.dic_imag['begin'] = atrib.get('begin', "")
            self.dic_imag['dur'] = atrib.get('dur', "")
            self.listaa.append(self.dic_imag.copy())

        elif name == 'sonido':

            self.dic_sonido['eti'] = name
            self.dic_sonido['source'] = atrib.get('source', "")
            self.dic_sonido['begin'] = atrib.get('begin', "")
            self.dic_sonido['dur'] = atrib.get('dur', "")
            self.listaa.append(self.dic_sonido.copy())

        elif name == 'texts':

            self.dic_texts['eti'] = name
            self.dic_texts['source'] = atrib.get('source', "")
            self.dic_texts['zona'] = atrib.get('zona', "")
            self.listaa.append(self.dic_texts.copy())

    def get_tags(self):

        return self.listaa


if __name__ == "__main__":

    parser = make_parser()
    Handler = SmallSMILHandler()
    parser.setContentHandler(Handler)
    parser.parse(open('karaoke.smil'))
    print(Handler.get_tags())
