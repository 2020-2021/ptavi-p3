#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import json
from smallsmilhandler import SmallSMILHandler
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from urllib.request import urlretrieve


class KaraokeLocal(SmallSMILHandler):

    def __init__(self, leer):
        Handler = SmallSMILHandler()
        parser = make_parser()
        parser.setContentHandler(Handler)
        parser.parse(open(leer))
        self.archivo = Handler.get_tags()

    def __str__(self):
        
        for eti in self.archivo:
            
            lista = []
            for atributo, value in eti.item():
                
                if value != "":
                    
                    lista += (atributo, '="', value, '"', '\t')
            print(''.join(lista))

    def to_json(self, file):
        
        archivo_json = file.replace(".smil", ".json")
        with open(archivo_json, "w") as arch:
            json.dump(self.archivo, arch, indent=4)

    def do_local(self):
        for eti in self.archivo:
            for atributo, value in eti.item():
                if atributo == 'src':
                    enlace = value
                    if enlace.startswith('http://'):
                        nombreA = enlace.split('/')
                        enlaceD(enlace, nombreA[-1])
                        eti[atributo] = nombreA[-1]
                        


if __name__ == "__main__":
    if len( sys.argv ) != 2:
        sys.exit("fichero.smil")

    leer = sys.argv[1]

    eLkaraoke = KaraokeLocal(leer)
    eLkaraoke.__str__()
    eLkaraoke.to_json(leer)
    eLkaraoke.do_local()
    eLkaraoke.to_json('local.json')
    eLkaraoke.__str__()
